<libosinfo version="0.0.1">
  <os id="http://fedoraproject.org/fedora/23">
    <short-id>fedora23</short-id>
    <_name>Fedora 23</_name>
    <version>23</version>
    <_vendor>Fedora Project</_vendor>
    <family>linux</family>
    <distro>fedora</distro>
    <upgrades id="http://fedoraproject.org/fedora/22"/>
    <derives-from id="http://fedoraproject.org/fedora/22"/>

    <release-date>2015-11-03</release-date>

    <variant id="workstation">
      <_name>Fedora 23 Workstation</_name>
    </variant>
    <variant id="workstation-netinst">
      <_name>Fedora 23 Workstation</_name>
    </variant>
    <variant id="server">
      <_name>Fedora 23 Server</_name>
    </variant>
    <variant id="server-netinst">
      <_name>Fedora 23 Server</_name>
    </variant>

    <!-- Workstation -->

    <!-- Live -->
    <media arch="i686" live="true">
      <variant id="workstation"/>
      <url>http://download.fedoraproject.org/pub/fedora/linux/releases/23/Workstation/i386/iso/Fedora-Live-Workstation-i686-23-10.iso</url>
      <iso>
        <volume-id>Fedora-Live-WS-i686-23.*</volume-id>
        <system-id>LINUX</system-id>
      </iso>
      <kernel>isolinux/vmlinuz0</kernel>
      <initrd>isolinux/initrd0.img</initrd>
    </media>
    <media arch="x86_64" live="true">
      <variant id="workstation"/>
      <url>http://download.fedoraproject.org/pub/fedora/linux/releases/23/Workstation/x86_64/iso/Fedora-Live-Workstation-x86_64-23-10.iso</url>
      <iso>
        <volume-id>Fedora-Live-WS-x86_64-23.*</volume-id>
        <system-id>LINUX</system-id>
      </iso>
      <kernel>isolinux/vmlinuz0</kernel>
      <initrd>isolinux/initrd0.img</initrd>
    </media>

    <!-- Network Installer !-->
    <media arch="i686">
      <variant id="workstation-netinst"/>
      <url>http://download.fedoraproject.org/pub/fedora/linux/releases/23/Workstation/i386/iso/Fedora-Workstation-netinst-i386-23.iso</url>
      <iso>
        <volume-id>Fedora-WS-23(_[[:alpha:]]*)*-i386</volume-id>
        <system-id>LINUX</system-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="x86_64">
      <variant id="workstation-netinst"/>
      <url>http://download.fedoraproject.org/pub/fedora/linux/releases/23/Workstation/x86_64/iso/Fedora-Workstation-netinst-x86_64-23.iso</url>
      <iso>
        <volume-id>Fedora-WS-23(_[[:alpha:]]*)*-x86_64</volume-id>
        <system-id>LINUX</system-id>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    <!-- Server -->

    <!-- DVD -->
    <media arch="i686">
      <variant id="server"/>
      <url>http://download.fedoraproject.org/pub/fedora/linux/releases/23/Server/i386/iso/Fedora-Server-DVD-i386-23.iso</url>
      <iso>
        <volume-id>Fedora-S-23(_[[:alpha:]]*)*-i386</volume-id>
        <system-id>LINUX</system-id>
        <!-- We need volume-size to distinguish DVD from netiso here:

             https://fedorahosted.org/rel-eng/ticket/6173
        -->
        <volume-size>2254878720</volume-size>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="x86_64">
      <variant id="server"/>
      <url>http://download.fedoraproject.org/pub/fedora/linux/releases/23/Server/x86_64/iso/Fedora-Server-DVD-x86_64-23.iso</url>
      <iso>
        <volume-id>Fedora-S-23(_[[:alpha:]]*)*-x86_64</volume-id>
        <system-id>LINUX</system-id>
        <volume-size>2149087232</volume-size>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    <!-- Network Installer !-->
    <media arch="i686">
      <variant id="server-netinst"/>
      <url>http://download.fedoraproject.org/pub/fedora/linux/releases/23/Server/i386/iso/Fedora-Server-netinst-i386-23.iso</url>
      <iso>
        <volume-id>Fedora-S-23(_[[:alpha:]]*)*-i386</volume-id>
        <system-id>LINUX</system-id>
        <volume-size>479557632</volume-size>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>
    <media arch="x86_64">
      <variant id="server-netinst"/>
      <url>http://download.fedoraproject.org/pub/fedora/linux/releases/23/Server/x86_64/iso/Fedora-Server-netinst-x86_64-23.iso</url>
      <iso>
        <volume-id>Fedora-S-23(_[[:alpha:]]*)*-x86_64</volume-id>
        <system-id>LINUX</system-id>
        <volume-size>435079168</volume-size>
      </iso>
      <kernel>isolinux/vmlinuz</kernel>
      <initrd>isolinux/initrd.img</initrd>
    </media>

    <tree arch="i686">
      <variant id="server"/>
      <url>http://download.fedoraproject.org/pub/fedora/linux/releases/23/Server/i386/os/</url>
      <treeinfo>
        <family>Fedora</family>
        <version>23</version>
        <arch>i386</arch>
      </treeinfo>
    </tree>
    <tree arch="x86_64">
      <url>http://download.fedoraproject.org/pub/fedora/linux/releases/23/Server/x86_64/os/</url>
      <variant id="server"/>
      <treeinfo>
        <family>Fedora</family>
        <version>23</version>
        <arch>x86_64</arch>
      </treeinfo>
    </tree>

    <!-- No install media or tree is provided for Fedora 23 Cloud variant -->

    <resources arch="all">
      <minimum>
        <n-cpus>1</n-cpus>
        <cpu>1000000000</cpu>
        <ram>1073741824</ram>
        <storage>10737418240</storage>
      </minimum>

      <recommended>
        <ram>2147483648</ram>
        <storage>21474836480</storage>
      </recommended>
    </resources>

    <installer>
      <script id='http://fedoraproject.org/fedora/kickstart/jeos'/>
      <script id='http://fedoraproject.org/fedora/kickstart/desktop'/>
    </installer>
  </os>
</libosinfo>
